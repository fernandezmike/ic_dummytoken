﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LM_JWT_Authentication.Entities
{
    public class TokenObject
    {
        public string Token { get; set; }
    }
}