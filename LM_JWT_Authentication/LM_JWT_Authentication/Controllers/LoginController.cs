﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using LM_JWT_Authentication.Models;
using LM_JWT_Authentication.Entities;
using System.Web.Http.Description;

namespace LM_JWT_Authentication.Controllers
{
    public class LoginController : ApiController
    {
        [HttpPost]
        [ResponseType(typeof(TokenObject))]
        public IHttpActionResult Authenticate([FromBody] LoginRequest login)
        {
            var loginResponse = new LoginResponse { };
            LoginRequest loginrequest = new LoginRequest { };
            loginrequest.Username = login.Username.ToLower();
            loginrequest.Password = login.Password;

            IHttpActionResult response;
            HttpResponseMessage responseMsg = new HttpResponseMessage();
            bool isUsernamePasswordValid = false;       

            if(login != null)
            isUsernamePasswordValid=loginrequest.Username =="admin" ? true:false;
            // if credentials are valid
            if (isUsernamePasswordValid)
            {
                TokenObject obj = new TokenObject();
                obj.Token = createToken(loginrequest.Username);
                
                //return the token
                return Ok(obj);
            }
            else
            {
                // if credentials are not valid send unauthorized status code in response
                loginResponse.responseMsg.StatusCode = HttpStatusCode.Unauthorized;
                response = ResponseMessage(loginResponse.responseMsg);
                return response;
            }
        }

        private string createToken(string username)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddMinutes(60);
            
            var tokenHandler = new JwtSecurityTokenHandler();
           
            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.SerialNumber, "1234"),
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Role, "Admin")            
            });

            const string sec = "as81feguow649bclb2c2h3od1lrz23fi2tq7dc8sykmdl4pmx7uk0onsfkhnu2wm2sz5uxnynfiyaxqdaqn14gza6mnqupimn8y7brcpxktdnqgn6tu1fjhbbu8wqgo3";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey,Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer:"http://localhost:50191",audience:"http://localhost:50191",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
}
